import os
import time

create_slice = 'kubectl exec -it deployment/onos-cli -n riab -- onos rsm create slice --e2NodeID e2:4/e00/3/c8 --scheduler RR --sliceID 2 --weight 60 --sliceType DL'
set_association = 'kubectl exec -it deployment/onos-cli -n riab -- onos rsm set association --dlSliceID 2 --e2NodeID e2:4/e00/3/c8 --drbID 5 --DuUeF1apID 29126'
delete_slice = 'kubectl exec -it deployment/onos-cli -n riab -- onos rsm delete slice --e2NodeID e2:4/e00/3/c8 --sliceID 2 --sliceType DL'

try:
    while True:
        print('Put UE 1 on Network Slice 2')
        os.system(create_slice)
        os.system(set_association)
        time.sleep (10)
        print('Put UE 1 on the default Network Slice')
        os.system(delete_slice)
        time.sleep (10)
except KeyboardInterrupt:
    print('Program interrupted')